package com.kevin;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {


    @JmsListener(destination = "queue-main")
    public void receiveMessage(String message){
        System.out.println("Message received is: " + message);

    }

}
