package com.kevin;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

@EnableJms
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        Sender sender = context.getBean(Sender.class);

        sender.sendMessage("queue-main", "First Message using Spring JMS!");
    }

    @Bean
    public JmsListenerContainerFactory mainFactory(ConnectionFactory factory, DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory containerFactory = new DefaultJmsListenerContainerFactory();
        configurer.configure(containerFactory, factory);

        return containerFactory;
    }

    @Bean // Create ActiveMQConnectionFactory
    public ActiveMQConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
        return connectionFactory;
    }

    @Bean // Create JmsTemplate
    public JmsTemplate jmsTemplate(){
        return new JmsTemplate(connectionFactory());
    }

    // Al parecer este metodo se esta ignorando, sigue usando el listener definido anteriormente
    @Bean
    public DefaultJmsListenerContainerFactory defaultJmsListenerContainerFactory(){
        System.out.println("Setting DefaultJmsListenerContainerFactory ...");
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrency("1-1");
        return factory;
    }


    // Si el gradle es version 5.x se puede ejecutar el sgte comando:
    // gradle clean build && java -jar b build/libs/jms-basic-connection-1.0-SNAPSHOT.jar
    // por que sino puede tirar error al ejecutar algunas tareas gradle

    // Si el gradle es menor a la version 5.x (ex. 4.10) se puede usar:
    // ./gradlew clean build && java -jar b build/libs/jms-basic-connection-1.0-SNAPSHOT.jar

    // Referencia
    // https://docs.gradle.org/4.10/userguide/command_line_interface.html#sec:command_line_warnings

    // Al parecer tampoco es necesario definir un DefaultJmsListenerContainerFactory,
    // ya que al remove el metodo defaultJmsListenerContainerFactory y/o mainFactory,
    // el envio de mensajes aun se realiza

}
